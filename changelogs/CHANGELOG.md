
## 0.2.1 [12-13-2022]

* Fixed API call to create DNS records

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!4

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!3

---