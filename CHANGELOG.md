
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:53PM

See merge request itentialopensource/adapters/adapter-ns1_cloud!16

---

## 0.5.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-ns1_cloud!14

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:12PM

See merge request itentialopensource/adapters/adapter-ns1_cloud!13

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:26PM

See merge request itentialopensource/adapters/adapter-ns1_cloud!12

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!11

---

## 0.4.5 [03-28-2024]

* Changes made at 2024.03.28_13:22PM

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!10

---

## 0.4.4 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!9

---

## 0.4.3 [03-11-2024]

* Changes made at 2024.03.11_15:35PM

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!8

---

## 0.4.2 [02-28-2024]

* Changes made at 2024.02.28_11:50AM

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!7

---

## 0.4.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!6

---

## 0.4.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!5

---

## 0.3.0 [12-07-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!5

---

## 0.2.1 [12-13-2022]

* Fixed API call to create DNS records

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!4

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!3

---

## 0.1.2 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!2

---

## 0.1.1 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-ns1_cloud!1

---
