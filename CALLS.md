## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for NS1. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for NS1.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the NS1 Cloud. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getZones(callback)</td>
    <td style="padding:15px">View a list of active zones</td>
    <td style="padding:15px">{base_path}/{version}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZonesZone(zone, callback)</td>
    <td style="padding:15px">View zone details</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZonesZone(zone, body, callback)</td>
    <td style="padding:15px">Modify a zone</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putZonesZone(zone, body, callback)</td>
    <td style="padding:15px">Create a new zone</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAZoneAllAssociatedRecords(zone, callback)</td>
    <td style="padding:15px">Delete a zone & all associated records</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZonesZoneDnssec(zone, callback)</td>
    <td style="padding:15px">View DNSSEC details for a zone</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}/dnssec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportZonefileZone(zone, callback)</td>
    <td style="padding:15px">View status of zone file import</td>
    <td style="padding:15px">{base_path}/{version}/import/zonefile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putImportZonefileZone(zone, zoneFile, async, callback)</td>
    <td style="padding:15px">Import a zone from a zone file</td>
    <td style="padding:15px">{base_path}/{version}/import/zonefile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearch(q, max, type = 'zone', callback)</td>
    <td style="padding:15px">Search zones and records</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(callback)</td>
    <td style="padding:15px">List available networks</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZonesZoneDomainType(zone, domain, type, callback)</td>
    <td style="padding:15px">View record configuration details</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZonesZoneDomainType(zone, domain, type, body, callback)</td>
    <td style="padding:15px">Update record configuration</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putZonesZoneDomainType(zone, domain, type, body, callback)</td>
    <td style="padding:15px">Create a new DNS record</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZonesZoneDomainType(zone, domain, type, callback)</td>
    <td style="padding:15px">Delete a record</td>
    <td style="padding:15px">{base_path}/{version}/zones/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetatypes(callback)</td>
    <td style="padding:15px">View available metadata</td>
    <td style="padding:15px">{base_path}/{version}/metatypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiltertypes(callback)</td>
    <td style="padding:15px">View available filters</td>
    <td style="padding:15px">{base_path}/{version}/filtertypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetatypesGeo(callback)</td>
    <td style="padding:15px">View available geographic metadata</td>
    <td style="padding:15px">{base_path}/{version}/metatypes/geo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewAvailableDataSourcesTypes(callback)</td>
    <td style="padding:15px">View available data sources types</td>
    <td style="padding:15px">{base_path}/{version}/data/sourcetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSources(callback)</td>
    <td style="padding:15px">View active data sources</td>
    <td style="padding:15px">{base_path}/{version}/data/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDataSources(config, callback)</td>
    <td style="padding:15px">Connect a new data source</td>
    <td style="padding:15px">{base_path}/{version}/data/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSourcesSourceid(sourceid, callback)</td>
    <td style="padding:15px">View a single data source</td>
    <td style="padding:15px">{base_path}/{version}/data/sources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDataSourcesSourceid(sourceid, body, callback)</td>
    <td style="padding:15px">Modify a data source</td>
    <td style="padding:15px">{base_path}/{version}/data/sources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataSourcesSourceid(sourceid, callback)</td>
    <td style="padding:15px">Delete a data source</td>
    <td style="padding:15px">{base_path}/{version}/data/sources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataFeedsSourceid(sourceid, callback)</td>
    <td style="padding:15px">View active data feeds for a source</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDataFeedsSourceid(sourceid, body, callback)</td>
    <td style="padding:15px">Publish data from a data source</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDataFeedsSourceid(sourceid, body, callback)</td>
    <td style="padding:15px">Connect a new data feed to a data source</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataFeedsSourceidFeedid(sourceid, feedid, callback)</td>
    <td style="padding:15px">View data feed details</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyADataFeed(sourceid, feedid, dataFeed, callback)</td>
    <td style="padding:15px">Modify a data feed</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataFeedsSourceidFeedid(sourceid, feedid, callback)</td>
    <td style="padding:15px">Disconnect a data feed</td>
    <td style="padding:15px">{base_path}/{version}/data/feeds/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringJobs(callback)</td>
    <td style="padding:15px">List monitoring jobs</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitoringJobs(body, callback)</td>
    <td style="padding:15px">Create a new monitoring job</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringJobsJobid(jobid, callback)</td>
    <td style="padding:15px">View monitoring job details</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitoringJobsJobid(jobid, body, callback)</td>
    <td style="padding:15px">Update details of a monitoring job</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAMonitoringJob(jobid, callback)</td>
    <td style="padding:15px">Remove a monitoring job</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringHistoryJobid(jobid, start, end, period = '1h', limit, region, exact, callback)</td>
    <td style="padding:15px">View historical status of monitoring job</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/history/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringMetricJobid(jobid, region, metric, period = '1h', callback)</td>
    <td style="padding:15px">View historical metrics for monitoring job</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metric/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringJobtypes(callback)</td>
    <td style="padding:15px">List available monitoring job types</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/jobtypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringRegions(callback)</td>
    <td style="padding:15px">List available monitoring regions</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLists(callback)</td>
    <td style="padding:15px">List all notification lists</td>
    <td style="padding:15px">{base_path}/{version}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLists(list, callback)</td>
    <td style="padding:15px">Create a new notification list</td>
    <td style="padding:15px">{base_path}/{version}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListsListid(listid, callback)</td>
    <td style="padding:15px">View notification list details</td>
    <td style="padding:15px">{base_path}/{version}/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListsListid(listid, list, callback)</td>
    <td style="padding:15px">Update a notification list</td>
    <td style="padding:15px">{base_path}/{version}/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteListsListid(listid, callback)</td>
    <td style="padding:15px">Remove a notification list</td>
    <td style="padding:15px">{base_path}/{version}/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotifytypes(callback)</td>
    <td style="padding:15px">List available notification types</td>
    <td style="padding:15px">{base_path}/{version}/notifytypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountSettings(callback)</td>
    <td style="padding:15px">View account contact details and settings</td>
    <td style="padding:15px">{base_path}/{version}/account/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountSettings(accountDetails, callback)</td>
    <td style="padding:15px">Modify contact details and settings</td>
    <td style="padding:15px">{base_path}/{version}/account/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountUsagewarnings(callback)</td>
    <td style="padding:15px">View overage alert settings</td>
    <td style="padding:15px">{base_path}/{version}/account/usagewarnings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountUsagewarnings(accountDetails, callback)</td>
    <td style="padding:15px">Modify overage alerting settings</td>
    <td style="padding:15px">{base_path}/{version}/account/usagewarnings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountUsers(callback)</td>
    <td style="padding:15px">View all account users</td>
    <td style="padding:15px">{base_path}/{version}/account/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountUsersUsername(username, callback)</td>
    <td style="padding:15px">View user details</td>
    <td style="padding:15px">{base_path}/{version}/account/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAUserSDetails(username, body, callback)</td>
    <td style="padding:15px">Update a user's details</td>
    <td style="padding:15px">{base_path}/{version}/account/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToAccount(username, body, callback)</td>
    <td style="padding:15px">Add user to account</td>
    <td style="padding:15px">{base_path}/{version}/account/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountUsersUsername(username, callback)</td>
    <td style="padding:15px">Remove a user</td>
    <td style="padding:15px">{base_path}/{version}/account/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountReinviteUsername(username, user, callback)</td>
    <td style="padding:15px">Resend invitation to a user</td>
    <td style="padding:15px">{base_path}/{version}/account/reinvite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountApikeys(callback)</td>
    <td style="padding:15px">View all API keys</td>
    <td style="padding:15px">{base_path}/{version}/account/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountApikeys(key, callback)</td>
    <td style="padding:15px">Create a new API Key</td>
    <td style="padding:15px">{base_path}/{version}/account/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountApikeysKeyid(keyid, callback)</td>
    <td style="padding:15px">View API key details</td>
    <td style="padding:15px">{base_path}/{version}/account/apikeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountApikeysKeyid(keyid, key, callback)</td>
    <td style="padding:15px">Modify details of an API Key</td>
    <td style="padding:15px">{base_path}/{version}/account/apikeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAPIKey(keyid, callback)</td>
    <td style="padding:15px">Delete an API Key</td>
    <td style="padding:15px">{base_path}/{version}/account/apikeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountTeams(callback)</td>
    <td style="padding:15px">View all teams</td>
    <td style="padding:15px">{base_path}/{version}/account/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountTeams(team, callback)</td>
    <td style="padding:15px">Create a new Team</td>
    <td style="padding:15px">{base_path}/{version}/account/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountTeamsTeamid(teamid, callback)</td>
    <td style="padding:15px">View team details</td>
    <td style="padding:15px">{base_path}/{version}/account/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountTeamsTeamid(teamid, team, callback)</td>
    <td style="padding:15px">Modify details of a Team</td>
    <td style="padding:15px">{base_path}/{version}/account/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountTeamsTeamid(teamid, callback)</td>
    <td style="padding:15px">Delete a Team</td>
    <td style="padding:15px">{base_path}/{version}/account/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountWhitelist(callback)</td>
    <td style="padding:15px">View all IP whitelists</td>
    <td style="padding:15px">{base_path}/{version}/account/whitelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountWhitelist(whitelist, callback)</td>
    <td style="padding:15px">Create a new IP Whitelist Record</td>
    <td style="padding:15px">{base_path}/{version}/account/whitelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountWhitelistId(id, callback)</td>
    <td style="padding:15px">View IP whitelist record details</td>
    <td style="padding:15px">{base_path}/{version}/account/whitelist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountWhitelistId(id, whitelist, callback)</td>
    <td style="padding:15px">Modify details of an IP Whitelist Record</td>
    <td style="padding:15px">{base_path}/{version}/account/whitelist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountWhitelistId(id, callback)</td>
    <td style="padding:15px">Delete an IP Whitelist Record</td>
    <td style="padding:15px">{base_path}/{version}/account/whitelist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPlantypes(callback)</td>
    <td style="padding:15px">View self-service subscription plans</td>
    <td style="padding:15px">{base_path}/{version}/account/plantypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPlan(callback)</td>
    <td style="padding:15px">View current subscription plan details</td>
    <td style="padding:15px">{base_path}/{version}/account/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountPlan(plan, callback)</td>
    <td style="padding:15px">Change your subscription plan</td>
    <td style="padding:15px">{base_path}/{version}/account/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountInvoices(callback)</td>
    <td style="padding:15px">List invoices</td>
    <td style="padding:15px">{base_path}/{version}/account/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadAnInvoice(invoiceid, authtoken, callback)</td>
    <td style="padding:15px">Download an invoice</td>
    <td style="padding:15px">{base_path}/{version}/account/invoices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountBillataglance(callback)</td>
    <td style="padding:15px">View bill at a glance</td>
    <td style="padding:15px">{base_path}/{version}/account/billataglance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPaymentmethods(callback)</td>
    <td style="padding:15px">View list of active payment methods</td>
    <td style="padding:15px">{base_path}/{version}/account/paymentmethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addANewPaymentMethod(paymentMethod, callback)</td>
    <td style="padding:15px">Add a new payment method</td>
    <td style="padding:15px">{base_path}/{version}/account/paymentmethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPaymentmethodsId(id, callback)</td>
    <td style="padding:15px">View payment method details</td>
    <td style="padding:15px">{base_path}/{version}/account/paymentmethods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeAPaymentMethodTheDefault(id, callback)</td>
    <td style="padding:15px">Make a payment method the default</td>
    <td style="padding:15px">{base_path}/{version}/account/paymentmethods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAPaymentMethod(id, callback)</td>
    <td style="padding:15px">Delete a payment method</td>
    <td style="padding:15px">{base_path}/{version}/account/paymentmethods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountActivity(limit, start, end, resourceType, user, resourceId, callback)</td>
    <td style="padding:15px">View account activity log</td>
    <td style="padding:15px">{base_path}/{version}/account/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsQps(callback)</td>
    <td style="padding:15px">Account-wide DNS queries per second</td>
    <td style="padding:15px">{base_path}/{version}/stats/qps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsQpsZone(zone, callback)</td>
    <td style="padding:15px">Zone-level queries per second</td>
    <td style="padding:15px">{base_path}/{version}/stats/qps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsQpsZoneDomainType(zone, domain, type, callback)</td>
    <td style="padding:15px">Record-level queries per second</td>
    <td style="padding:15px">{base_path}/{version}/stats/qps/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsUsage(period = '1h', aggregate, byTier, callback)</td>
    <td style="padding:15px">View account-wide usage statistics</td>
    <td style="padding:15px">{base_path}/{version}/stats/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsUsageZone(zone, period = '1h', expand, aggregate, byTier, callback)</td>
    <td style="padding:15px">View zone-level usage statistics</td>
    <td style="padding:15px">{base_path}/{version}/stats/usage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewRecordLevelUsageStatistics(zone, domain, type, period = '1h', callback)</td>
    <td style="padding:15px">View record-level usage statistics</td>
    <td style="padding:15px">{base_path}/{version}/stats/usage/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarApps(callback)</td>
    <td style="padding:15px">List pulsar apps</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarAppsAppID(appID, callback)</td>
    <td style="padding:15px">View application details</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarAppsAppIDJobs(appID, callback)</td>
    <td style="padding:15px">Get list of all jobs for an App</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarAppsAppIDJobsJobID(appID, jobID, callback)</td>
    <td style="padding:15px">View job details</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarAppsAppIDJobsJobIDData(appID, jobID, start, end, period, geo, agg, asn, callback)</td>
    <td style="padding:15px">View aggregated performance data</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps/{pathv1}/jobs/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarAppsAppIDJobsJobIDAvailability(appID, jobID, callback)</td>
    <td style="padding:15px">View availability data</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/apps/{pathv1}/jobs/{pathv2}/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewDecisionsForAccount(start, end, period, geo, agg, asn, callback)</td>
    <td style="padding:15px">View decisions for account</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/query/decision/customer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewInsufficientDecisionDataForAccount(start, end, period, geo, agg, asn, callback)</td>
    <td style="padding:15px">View insufficient decision data for account</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/query/decision/customer/undetermined?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarQueryDecisionRecordDomainType(domain, type, start, end, period, geo, agg, asn, callback)</td>
    <td style="padding:15px">View decisions for record</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/query/decision/record/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPulsarQueryDecisionRecordUndeterminedDomainRectype(domain, rectype, start, end, period, geo, agg, asn, callback)</td>
    <td style="padding:15px">View insufficient decision data for account</td>
    <td style="padding:15px">{base_path}/{version}/pulsar/query/decision/record/undetermined/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
