# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Ns1_cloud System. The API that was used to build the adapter for Ns1_cloud is usually available in the report directory of this adapter. The adapter utilizes the Ns1_cloud API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The NS1 Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with NS1 Cloud. With this adapter you have the ability to perform operations such as:

- Traffic routing
- Zone
- Records
- Pulsar

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
