/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ns1_cloud',
      type: 'Ns1Cloud',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Ns1Cloud = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Ns1_cloud Adapter Test', () => {
  describe('Ns1Cloud Class Tests', () => {
    const a = new Ns1Cloud(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let zonesZone = 'fakedata';
    const zonesPostZonesZoneBodyParam = {
      id: 'string',
      ttl: 6,
      nx_ttl: 10,
      retry: 10,
      zone: 'string',
      refresh: 8,
      expiry: 10,
      dns_servers: [
        'string'
      ],
      networks: [
        6
      ],
      network_pools: [
        'string'
      ],
      meta: {},
      hostmaster: 'string',
      dnssec: true
    };
    describe('#postZonesZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postZonesZone(zonesZone, zonesPostZonesZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.ttl);
                assert.equal(6, data.response.nx_ttl);
                assert.equal(10, data.response.retry);
                assert.equal('string', data.response.zone);
                assert.equal(3, data.response.refresh);
                assert.equal(2, data.response.expiry);
                assert.equal(true, Array.isArray(data.response.dns_servers));
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.network_pools));
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.hostmaster);
                assert.equal(false, data.response.dnssec);
              } else {
                runCommonAsserts(data, error);
              }
              zonesZone = data.response.zone;
              saveMockData('Zones', 'postZonesZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putImportZonefileZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putImportZonefileZone(zonesZone, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'putImportZonefileZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportZonefileZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImportZonefileZone(zonesZone, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getImportZonefileZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearch(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZones((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesPutZonesZoneBodyParam = {
      id: 'string',
      ttl: 1,
      nx_ttl: 6,
      retry: 10,
      zone: 'string',
      refresh: 7,
      expiry: 1,
      dns_servers: [
        'string'
      ],
      networks: [
        9
      ],
      network_pools: [
        'string'
      ],
      meta: {},
      hostmaster: 'string',
      dnssec: true
    };
    describe('#putZonesZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putZonesZone(zonesZone, zonesPutZonesZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'putZonesZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZonesZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZonesZone(zonesZone, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(2, data.response.ttl);
                assert.equal(8, data.response.nx_ttl);
                assert.equal(4, data.response.retry);
                assert.equal('string', data.response.zone);
                assert.equal(6, data.response.refresh);
                assert.equal(7, data.response.expiry);
                assert.equal(true, Array.isArray(data.response.dns_servers));
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.network_pools));
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.hostmaster);
                assert.equal(false, data.response.dnssec);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getZonesZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZonesZoneDnssec - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZonesZoneDnssec(zonesZone, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getZonesZoneDnssec', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let recordsZone = 'fakedata';
    let recordsDomain = 'fakedata';
    let recordsType = 'fakedata';
    const recordsPostZonesZoneDomainTypeBodyParam = {
      id: 'string',
      networks: [
        1
      ],
      type: 'string',
      feeds: [
        {}
      ],
      tier: 10,
      customer: 4,
      domain: 'string',
      zone: 'string',
      answers: [
        {
          id: 'string',
          answer: [
            'string'
          ]
        }
      ],
      regions: {},
      meta: {},
      filters: [
        {}
      ],
      ttl: 7,
      use_client_subnet: false
    };
    describe('#postZonesZoneDomainType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postZonesZoneDomainType(recordsZone, recordsDomain, recordsType, recordsPostZonesZoneDomainTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.feeds));
                assert.equal(3, data.response.tier);
                assert.equal(4, data.response.customer);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.zone);
                assert.equal(true, Array.isArray(data.response.answers));
                assert.equal('object', typeof data.response.regions);
                assert.equal('object', typeof data.response.meta);
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(3, data.response.ttl);
                assert.equal(true, data.response.use_client_subnet);
              } else {
                runCommonAsserts(data, error);
              }
              recordsZone = data.response.zone;
              recordsDomain = data.response.domain;
              recordsType = data.response.type;
              saveMockData('Records', 'postZonesZoneDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFiltertypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFiltertypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getFiltertypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetatypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMetatypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getMetatypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetatypesGeo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMetatypesGeo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getMetatypesGeo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsPutZonesZoneDomainTypeBodyParam = {
      id: 'string',
      networks: [
        1
      ],
      type: 'string',
      feeds: [
        {}
      ],
      tier: 1,
      customer: 5,
      domain: 'string',
      zone: 'string',
      answers: [
        {
          id: 'string',
          answer: [
            'string'
          ]
        }
      ],
      regions: {},
      meta: {},
      filters: [
        {}
      ],
      ttl: 6,
      use_client_subnet: true
    };
    describe('#putZonesZoneDomainType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putZonesZoneDomainType(recordsZone, recordsDomain, recordsType, recordsPutZonesZoneDomainTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'putZonesZoneDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZonesZoneDomainType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZonesZoneDomainType(recordsZone, recordsDomain, recordsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.feeds));
                assert.equal(7, data.response.tier);
                assert.equal(10, data.response.customer);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.zone);
                assert.equal(true, Array.isArray(data.response.answers));
                assert.equal('object', typeof data.response.regions);
                assert.equal('object', typeof data.response.meta);
                assert.equal(true, Array.isArray(data.response.filters));
                assert.equal(10, data.response.ttl);
                assert.equal(true, data.response.use_client_subnet);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getZonesZoneDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataSourcesSourceid = 'fakedata';
    const dataSourcesPostDataSourcesSourceidBodyParam = {
      sourcetype: 'string',
      id: 'string',
      config: {
        api_key: 'string',
        secret_key: 'string',
        webhook_token: 'string'
      },
      feeds: [
        {}
      ],
      name: 'string',
      status: 'string'
    };
    describe('#postDataSourcesSourceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDataSourcesSourceid(dataSourcesSourceid, dataSourcesPostDataSourcesSourceidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.sourcetype);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.feeds));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'postDataSourcesSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataSourcesPutDataSourcesBodyParam = {
      name: 'string',
      config: 'string',
      sourcetype: 'string'
    };
    describe('#putDataSources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDataSources(dataSourcesPutDataSourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'putDataSources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSources((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'getDataSources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSourcesSourceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSourcesSourceid(dataSourcesSourceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.sourcetype);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.feeds));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'getDataSourcesSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#viewAvailableDataSourcesTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.viewAvailableDataSourcesTypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'viewAvailableDataSourcesTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataFeedsSourceid = 'fakedata';
    const dataFeedsPostDataFeedsSourceidBodyParam = {};
    describe('#postDataFeedsSourceid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDataFeedsSourceid(dataFeedsSourceid, dataFeedsPostDataFeedsSourceidBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'postDataFeedsSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataFeedsFeedid = 'fakedata';
    const dataFeedsModifyADataFeedBodyParam = {
      destinations: [
        {
          destid: 'string',
          desttype: 'string',
          record: 'string'
        }
      ],
      id: 'string',
      data: {
        up: 'string'
      },
      config: {
        label: 'string'
      },
      name: 'string'
    };
    describe('#modifyADataFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyADataFeed(dataFeedsSourceid, dataFeedsFeedid, dataFeedsModifyADataFeedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.destinations));
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.data);
                assert.equal('object', typeof data.response.config);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'modifyADataFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataFeedsPutDataFeedsSourceidBodyParam = {
      destinations: [
        {
          destid: 'string',
          desttype: 'string',
          record: 'string'
        }
      ],
      id: 'string',
      data: {
        up: 'string'
      },
      config: {
        label: 'string'
      },
      name: 'string'
    };
    describe('#putDataFeedsSourceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDataFeedsSourceid(dataFeedsSourceid, dataFeedsPutDataFeedsSourceidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'putDataFeedsSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataFeedsSourceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataFeedsSourceid(dataFeedsSourceid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'getDataFeedsSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataFeedsSourceidFeedid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataFeedsSourceidFeedid(dataFeedsSourceid, dataFeedsFeedid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.destinations));
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.data);
                assert.equal('object', typeof data.response.config);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'getDataFeedsSourceidFeedid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitoringJobsJobid = 'fakedata';
    const monitoringJobsPostMonitoringJobsJobidBodyParam = {};
    describe('#postMonitoringJobsJobid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringJobsJobid(monitoringJobsJobid, monitoringJobsPostMonitoringJobsJobidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.notes);
                assert.equal('string', data.response.policy);
                assert.equal(10, data.response.frequency);
                assert.equal(true, Array.isArray(data.response.regions));
                assert.equal('string', data.response.region_scope);
                assert.equal(false, data.response.active);
                assert.equal(false, data.response.rapid_recheck);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.job_type);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.rules));
                assert.equal(10, data.response.notify_delay);
                assert.equal(1, data.response.notify_repeat);
                assert.equal(true, data.response.notify_failback);
                assert.equal(false, data.response.notify_regional);
                assert.equal('object', typeof data.response.notify_list);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'postMonitoringJobsJobid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringHistoryJobid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringHistoryJobid(monitoringJobsJobid, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringHistoryJobid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitoringJobsPutMonitoringJobsBodyParam = {
      status: {
        global: {
          since: 3,
          status: 'string'
        },
        ams: {
          since: 9,
          status: 'string'
        }
      },
      notes: {},
      policy: 'string',
      frequency: 4,
      regions: [
        'string'
      ],
      region_scope: 'string',
      active: true,
      rapid_recheck: true,
      name: 'string',
      job_type: 'string',
      config: {
        ssl: 'string',
        send: 'string',
        port: 8,
        host: 'string'
      },
      rules: [
        {
          value: 'string',
          comparison: 'string',
          key: 'string'
        }
      ],
      notify_delay: 2,
      notify_repeat: 5,
      notify_failback: true,
      notify_regional: true,
      notify_list: {},
      id: 'string'
    };
    describe('#putMonitoringJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putMonitoringJobs(monitoringJobsPutMonitoringJobsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'putMonitoringJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringJobs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringJobsJobid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringJobsJobid(monitoringJobsJobid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.notes);
                assert.equal('string', data.response.policy);
                assert.equal(10, data.response.frequency);
                assert.equal(true, Array.isArray(data.response.regions));
                assert.equal('string', data.response.region_scope);
                assert.equal(true, data.response.active);
                assert.equal(true, data.response.rapid_recheck);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.job_type);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.rules));
                assert.equal(10, data.response.notify_delay);
                assert.equal(6, data.response.notify_repeat);
                assert.equal(true, data.response.notify_failback);
                assert.equal(false, data.response.notify_regional);
                assert.equal('object', typeof data.response.notify_list);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringJobsJobid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringJobtypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringJobtypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tcp);
                assert.equal('object', typeof data.response.ping);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringJobtypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringMetricJobid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringMetricJobid(monitoringJobsJobid, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringMetricJobid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringRegions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMonitoringRegions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'getMonitoringRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationListsListid = 'fakedata';
    const notificationListsPostListsListidBodyParam = {
      id: 'string',
      name: 'string',
      notify_list: [
        {
          config: {
            email: 'string'
          },
          type: 'string'
        }
      ]
    };
    describe('#postListsListid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postListsListid(notificationListsListid, notificationListsPostListsListidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.notify_list));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'postListsListid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationListsPutListsBodyParam = {};
    describe('#putLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putLists(notificationListsPutListsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'putLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLists((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'getLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListsListid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getListsListid(notificationListsListid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.notify_list));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'getListsListid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotifytypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNotifytypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
                assert.equal('object', typeof data.response.email);
                assert.equal('object', typeof data.response.datafeed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'getNotifytypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementGeneralPostAccountSettingsBodyParam = {
      email: 'string',
      address: {
        country: 'string',
        street: 'string',
        state: 'string',
        city: 'string',
        postalcode: 'string'
      },
      phone: 'string',
      customerid: 6,
      company: 'string',
      lastname: 'string',
      firstname: 'string'
    };
    describe('#postAccountSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountSettings(accountManagementGeneralPostAccountSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.email);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.phone);
                assert.equal(4, data.response.customerid);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.lastname);
                assert.equal('string', data.response.firstname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementGeneral', 'postAccountSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementGeneralPostAccountUsagewarningsBodyParam = {
      queries: {
        send_warnings: false,
        warning_2: 1,
        warning_1: 10
      },
      records: {
        send_warnings: true,
        warning_2: 10,
        warning_1: 6
      }
    };
    describe('#postAccountUsagewarnings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountUsagewarnings(accountManagementGeneralPostAccountUsagewarningsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queries);
                assert.equal('object', typeof data.response.records);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementGeneral', 'postAccountUsagewarnings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.email);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.phone);
                assert.equal(7, data.response.customerid);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.lastname);
                assert.equal('string', data.response.firstname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementGeneral', 'getAccountSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountUsagewarnings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountUsagewarnings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queries);
                assert.equal('object', typeof data.response.records);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementGeneral', 'getAccountUsagewarnings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementUserUsername = 'fakedata';
    const accountManagementUserPostAccountReinviteUsernameBodyParam = {
      permissions: {
        dns: {
          view_zones: false,
          manage_zones: true,
          zones_allow_by_default: false,
          zones_deny: [
            {}
          ],
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: true,
          manage_datasources: false,
          manage_datafeeds: false
        },
        account: {
          manage_payment_methods: false,
          manage_plan: false,
          manage_teams: true,
          manage_apikeys: false,
          manage_account_settings: true,
          view_activity_log: true,
          view_invoices: false,
          manage_users: false
        },
        monitoring: {
          manage_lists: false,
          manage_jobs: false,
          view_jobs: true
        }
      },
      teams: [
        {}
      ],
      email: 'string',
      last_access: null,
      notify: {
        billing: false
      },
      name: 'string',
      username: 'string',
      ip_whitelist_strict: true,
      ip_whitelist: [
        {}
      ]
    };
    describe('#postAccountReinviteUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAccountReinviteUsername(accountManagementUserUsername, accountManagementUserPostAccountReinviteUsernameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'postAccountReinviteUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementUserUpdateAUserSDetailsBodyParam = {
      permissions: {
        dns: {
          view_zones: false,
          manage_zones: false,
          zones_allow_by_default: false,
          zones_deny: [
            {}
          ],
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: true,
          manage_datasources: true,
          manage_datafeeds: false
        },
        account: {
          manage_payment_methods: false,
          manage_plan: false,
          manage_teams: true,
          manage_apikeys: false,
          manage_account_settings: false,
          view_activity_log: true,
          view_invoices: false,
          manage_users: true
        },
        monitoring: {
          manage_lists: true,
          manage_jobs: true,
          view_jobs: false
        }
      },
      teams: [
        {}
      ],
      email: 'string',
      last_access: null,
      notify: {
        billing: false
      },
      name: 'string',
      username: 'string',
      ip_whitelist_strict: false,
      ip_whitelist: [
        {}
      ]
    };
    describe('#updateAUserSDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAUserSDetails(accountManagementUserUsername, accountManagementUserUpdateAUserSDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal(true, Array.isArray(data.response.teams));
                assert.equal('string', data.response.email);
                assert.equal('object', typeof data.response.notify);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.username);
                assert.equal(false, data.response.ip_whitelist_strict);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'updateAUserSDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'getAccountUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementUserAddUserToAccountBodyParam = {
      permissions: {
        dns: {
          view_zones: true,
          manage_zones: true,
          zones_allow_by_default: true,
          zones_deny: [
            {}
          ],
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: false,
          manage_datasources: true,
          manage_datafeeds: true
        },
        account: {
          manage_payment_methods: false,
          manage_plan: true,
          manage_teams: true,
          manage_apikeys: false,
          manage_account_settings: true,
          view_activity_log: true,
          view_invoices: false,
          manage_users: true
        },
        monitoring: {
          manage_lists: false,
          manage_jobs: false,
          view_jobs: true
        }
      },
      teams: [
        {}
      ],
      email: 'string',
      last_access: null,
      notify: {
        billing: false
      },
      name: 'string',
      username: 'string',
      ip_whitelist_strict: false,
      ip_whitelist: [
        {}
      ]
    };
    describe('#addUserToAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addUserToAccount(accountManagementUserUsername, accountManagementUserAddUserToAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'addUserToAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountUsersUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountUsersUsername(accountManagementUserUsername, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal(true, Array.isArray(data.response.teams));
                assert.equal('string', data.response.email);
                assert.equal('object', typeof data.response.notify);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.username);
                assert.equal(true, data.response.ip_whitelist_strict);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'getAccountUsersUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementAPIKeyKeyid = 'fakedata';
    const accountManagementAPIKeyPostAccountApikeysKeyidBodyParam = {
      permissions: {
        dns: {
          zones_allow_by_default: true,
          view_zones: false,
          manage_zones: true,
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: true,
          manage_datasources: false,
          manage_datafeeds: true
        },
        account: {
          manage_payment_methods: false,
          manage_plan: false,
          manage_teams: false,
          manage_apikeys: true,
          manage_account_settings: true,
          view_activity_log: false,
          view_invoices: true,
          manage_users: false
        },
        monitoring: {
          manage_lists: false,
          manage_jobs: false,
          view_jobs: true
        }
      },
      teams: [
        {}
      ],
      id: 'string',
      last_access: null,
      key: 'string',
      name: 'string',
      ip_whitelist_strict: false,
      ip_whitelist: [
        {}
      ]
    };
    describe('#postAccountApikeysKeyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountApikeysKeyid(accountManagementAPIKeyKeyid, accountManagementAPIKeyPostAccountApikeysKeyidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal(true, Array.isArray(data.response.teams));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.ip_whitelist_strict);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementAPIKey', 'postAccountApikeysKeyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementAPIKeyPutAccountApikeysBodyParam = {
      permissions: {
        dns: {
          zones_allow_by_default: false,
          view_zones: false,
          manage_zones: true,
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: false,
          manage_datasources: true,
          manage_datafeeds: false
        },
        account: {
          manage_payment_methods: true,
          manage_plan: true,
          manage_teams: true,
          manage_apikeys: false,
          manage_account_settings: false,
          view_activity_log: true,
          view_invoices: false,
          manage_users: false
        },
        monitoring: {
          manage_lists: false,
          manage_jobs: false,
          view_jobs: false
        }
      },
      teams: [
        {}
      ],
      id: 'string',
      last_access: null,
      key: 'string',
      name: 'string',
      ip_whitelist_strict: true,
      ip_whitelist: [
        {}
      ]
    };
    describe('#putAccountApikeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountApikeys(accountManagementAPIKeyPutAccountApikeysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementAPIKey', 'putAccountApikeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountApikeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountApikeys((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementAPIKey', 'getAccountApikeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountApikeysKeyid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountApikeysKeyid(accountManagementAPIKeyKeyid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal(true, Array.isArray(data.response.teams));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.ip_whitelist_strict);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementAPIKey', 'getAccountApikeysKeyid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementTeamTeamid = 'fakedata';
    const accountManagementTeamPostAccountTeamsTeamidBodyParam = {
      permissions: {
        dns: {
          zones_allow_by_default: false,
          view_zones: true,
          manage_zones: true,
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: true,
          manage_datasources: true,
          manage_datafeeds: false
        },
        account: {
          manage_payment_methods: true,
          manage_plan: false,
          manage_teams: true,
          manage_apikeys: true,
          manage_account_settings: true,
          view_activity_log: true,
          view_invoices: true,
          manage_users: false
        },
        monitoring: {
          manage_lists: false,
          manage_jobs: true,
          view_jobs: true
        }
      },
      id: 'string',
      name: 'string',
      ip_whitelist: [
        {}
      ]
    };
    describe('#postAccountTeamsTeamid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountTeamsTeamid(accountManagementTeamTeamid, accountManagementTeamPostAccountTeamsTeamidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementTeam', 'postAccountTeamsTeamid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementTeamPutAccountTeamsBodyParam = {
      permissions: {
        dns: {
          zones_allow_by_default: false,
          view_zones: true,
          manage_zones: false,
          zones_allow: [
            {}
          ]
        },
        data: {
          push_to_datafeeds: false,
          manage_datasources: false,
          manage_datafeeds: false
        },
        account: {
          manage_payment_methods: false,
          manage_plan: true,
          manage_teams: false,
          manage_apikeys: false,
          manage_account_settings: true,
          view_activity_log: false,
          view_invoices: true,
          manage_users: false
        },
        monitoring: {
          manage_lists: true,
          manage_jobs: false,
          view_jobs: false
        }
      },
      id: 'string',
      name: 'string',
      ip_whitelist: [
        {}
      ]
    };
    describe('#putAccountTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountTeams(accountManagementTeamPutAccountTeamsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementTeam', 'putAccountTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountTeams((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementTeam', 'getAccountTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountTeamsTeamid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountTeamsTeamid(accountManagementTeamTeamid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.permissions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.ip_whitelist));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementTeam', 'getAccountTeamsTeamid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let accountManagementIPWhitelistId = 'fakedata';
    const accountManagementIPWhitelistPostAccountWhitelistIdBodyParam = {
      id: 'string',
      name: 'string',
      values: [
        {}
      ]
    };
    describe('#postAccountWhitelistId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountWhitelistId(accountManagementIPWhitelistId, accountManagementIPWhitelistPostAccountWhitelistIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              accountManagementIPWhitelistId = data.response.id;
              saveMockData('AccountManagementIPWhitelist', 'postAccountWhitelistId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementIPWhitelistPutAccountWhitelistBodyParam = {
      id: 'string',
      name: 'string',
      values: [
        {}
      ]
    };
    describe('#putAccountWhitelist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountWhitelist(accountManagementIPWhitelistPutAccountWhitelistBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementIPWhitelist', 'putAccountWhitelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountWhitelist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountWhitelist((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementIPWhitelist', 'getAccountWhitelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountWhitelistId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountWhitelistId(accountManagementIPWhitelistId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementIPWhitelist', 'getAccountWhitelistId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementPlanAndBillingPostAccountPlanBodyParam = {};
    describe('#postAccountPlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountPlan(accountManagementPlanAndBillingPostAccountPlanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.overage_order);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.balance);
                assert.equal('string', data.response.discount_type);
                assert.equal('string', data.response.monthly_price);
                assert.equal('object', typeof data.response.access_charges);
                assert.equal('string', data.response.period);
                assert.equal('string', data.response.discount);
                assert.equal('object', typeof data.response.included);
                assert.equal('string', data.response.recurring_cost);
                assert.equal('object', typeof data.response.overages);
                assert.equal('object', typeof data.response.monitoring);
                assert.equal(false, data.response.locked);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'postAccountPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountBillataglance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountBillataglance((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.plan);
                assert.equal('string', data.response.period);
                assert.equal(9, data.response.last_invoice);
                assert.equal(5, data.response.next_invoice);
                assert.equal(8, data.response.next_base_invoice);
                assert.equal('string', data.response.recurring_cost);
                assert.equal('string', data.response.recurring_cost_next_invoice);
                assert.equal('object', typeof data.response.any);
                assert.equal('object', typeof data.response.static);
                assert.equal('object', typeof data.response.dynamic);
                assert.equal('object', typeof data.response.intelligent);
                assert.equal('object', typeof data.response.totals);
                assert.equal('string', data.response.balance);
                assert.equal('string', data.response.bill);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'getAccountBillataglance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountInvoices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountInvoices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'getAccountInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementPlanAndBillingInvoiceid = 'fakedata';
    describe('#downloadAnInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadAnInvoice(accountManagementPlanAndBillingInvoiceid, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'downloadAnInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountPlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountPlan((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.overage_order);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.balance);
                assert.equal('string', data.response.discount_type);
                assert.equal('string', data.response.monthly_price);
                assert.equal('object', typeof data.response.access_charges);
                assert.equal('string', data.response.period);
                assert.equal('string', data.response.discount);
                assert.equal('object', typeof data.response.included);
                assert.equal('string', data.response.recurring_cost);
                assert.equal('object', typeof data.response.overages);
                assert.equal('object', typeof data.response.monitoring);
                assert.equal(true, data.response.locked);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'getAccountPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountPlantypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountPlantypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPlanAndBilling', 'getAccountPlantypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementPaymentMethodId = 'fakedata';
    describe('#makeAPaymentMethodTheDefault - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.makeAPaymentMethodTheDefault(accountManagementPaymentMethodId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPaymentMethod', 'makeAPaymentMethodTheDefault', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountManagementPaymentMethodAddANewPaymentMethodBodyParam = {
      type: 'string',
      default: true,
      cc_expire_year: 9,
      cc_expire_month: 9,
      firstname: 'string',
      lastname: 'string',
      company: 'string',
      cc_type: 'string',
      phone: 'string',
      address: {
        country: 'string',
        street: 'string',
        state: 'string',
        city: 'string',
        postalcode: 'string'
      },
      cc_number_last4: 'string',
      id: 6
    };
    describe('#addANewPaymentMethod - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addANewPaymentMethod(accountManagementPaymentMethodAddANewPaymentMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPaymentMethod', 'addANewPaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountPaymentmethods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountPaymentmethods((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPaymentMethod', 'getAccountPaymentmethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountPaymentmethodsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountPaymentmethodsId(accountManagementPaymentMethodId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.default);
                assert.equal(8, data.response.cc_expire_year);
                assert.equal(9, data.response.cc_expire_month);
                assert.equal('string', data.response.firstname);
                assert.equal('string', data.response.lastname);
                assert.equal('string', data.response.company);
                assert.equal('string', data.response.cc_type);
                assert.equal('string', data.response.phone);
                assert.equal('object', typeof data.response.address);
                assert.equal('string', data.response.cc_number_last4);
                assert.equal(1, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPaymentMethod', 'getAccountPaymentmethodsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountActivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountActivity(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementActivityLog', 'getAccountActivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsQps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsQps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.qps);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatisticsQPS', 'getStatsQps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statisticsQPSZone = 'fakedata';
    describe('#getStatsQpsZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsQpsZone(statisticsQPSZone, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.qps);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatisticsQPS', 'getStatsQpsZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statisticsQPSDomain = 'fakedata';
    const statisticsQPSType = 'fakedata';
    describe('#getStatsQpsZoneDomainType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsQpsZoneDomainType(statisticsQPSZone, statisticsQPSDomain, statisticsQPSType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.qps);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatisticsQPS', 'getStatsQpsZoneDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let statisticsUsageZone = 'fakedata';
    let statisticsUsageDomain = 'fakedata';
    let statisticsUsageType = 'fakedata';
    describe('#getStatsUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsUsage(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.zone);
                assert.equal(true, data.response.expand);
                assert.equal('1h', data.response.period);
                assert.equal(false, data.response.aggregate);
                assert.equal(false, data.response.byTier);
              } else {
                runCommonAsserts(data, error);
              }
              statisticsUsageZone = data.response.zone;
              statisticsUsageDomain = data.response.domain;
              statisticsUsageType = data.response.type;
              saveMockData('StatisticsUsage', 'getStatsUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsUsageZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsUsageZone(statisticsUsageZone, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.zone);
                assert.equal(true, data.response.expand);
                assert.equal('24h', data.response.period);
                assert.equal(true, data.response.aggregate);
                assert.equal(false, data.response.byTier);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatisticsUsage', 'getStatsUsageZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#viewRecordLevelUsageStatistics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.viewRecordLevelUsageStatistics(statisticsUsageZone, statisticsUsageDomain, statisticsUsageType, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.zone);
                assert.equal(true, data.response.expand);
                assert.equal('1h', data.response.period);
                assert.equal(false, data.response.aggregate);
                assert.equal(false, data.response.byTier);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatisticsUsage', 'viewRecordLevelUsageStatistics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPulsarApps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPulsarApps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarApp', 'getPulsarApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarAppAppID = 'fakedata';
    describe('#getPulsarAppsAppID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarAppsAppID(pulsarAppAppID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarApp', 'getPulsarAppsAppID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarJobAppID = 'fakedata';
    describe('#getPulsarAppsAppIDJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPulsarAppsAppIDJobs(pulsarJobAppID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarJob', 'getPulsarAppsAppIDJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarJobJobID = 'fakedata';
    describe('#getPulsarAppsAppIDJobsJobID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarAppsAppIDJobsJobID(pulsarJobAppID, pulsarJobJobID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarJob', 'getPulsarAppsAppIDJobsJobID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarPerformanceDataAppID = 'fakedata';
    const pulsarPerformanceDataJobID = 'fakedata';
    describe('#getPulsarAppsAppIDJobsJobIDData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarAppsAppIDJobsJobIDData(pulsarPerformanceDataAppID, pulsarPerformanceDataJobID, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarPerformanceData', 'getPulsarAppsAppIDJobsJobIDData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarAvailabilityAppID = 'fakedata';
    const pulsarAvailabilityJobID = 'fakedata';
    describe('#getPulsarAppsAppIDJobsJobIDAvailability - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarAppsAppIDJobsJobIDAvailability(pulsarAvailabilityAppID, pulsarAvailabilityJobID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarAvailability', 'getPulsarAppsAppIDJobsJobIDAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#viewDecisionsForAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.viewDecisionsForAccount(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarDecisions', 'viewDecisionsForAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#viewInsufficientDecisionDataForAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.viewInsufficientDecisionDataForAccount(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarDecisions', 'viewInsufficientDecisionDataForAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarDecisionsDomain = 'fakedata';
    const pulsarDecisionsRectype = 'fakedata';
    describe('#getPulsarQueryDecisionRecordUndeterminedDomainRectype - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarQueryDecisionRecordUndeterminedDomainRectype(pulsarDecisionsDomain, pulsarDecisionsRectype, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarDecisions', 'getPulsarQueryDecisionRecordUndeterminedDomainRectype', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pulsarDecisionsType = 'fakedata';
    describe('#getPulsarQueryDecisionRecordDomainType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPulsarQueryDecisionRecordDomainType(pulsarDecisionsDomain, pulsarDecisionsType, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PulsarDecisions', 'getPulsarQueryDecisionRecordDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAZoneAllAssociatedRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAZoneAllAssociatedRecords(zonesZone, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'deleteAZoneAllAssociatedRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZonesZoneDomainType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZonesZoneDomainType(recordsZone, recordsDomain, recordsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'deleteZonesZoneDomainType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataSourcesSourceid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDataSourcesSourceid(dataSourcesSourceid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataSources', 'deleteDataSourcesSourceid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataFeedsSourceidFeedid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDataFeedsSourceidFeedid(dataFeedsSourceid, dataFeedsFeedid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataFeeds', 'deleteDataFeedsSourceidFeedid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAMonitoringJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeAMonitoringJob(monitoringJobsJobid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitoringJobs', 'removeAMonitoringJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteListsListid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteListsListid(notificationListsListid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationLists', 'deleteListsListid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountUsersUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountUsersUsername(accountManagementUserUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementUser', 'deleteAccountUsersUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAPIKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnAPIKey(accountManagementAPIKeyKeyid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementAPIKey', 'deleteAnAPIKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountTeamsTeamid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountTeamsTeamid(accountManagementTeamTeamid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementTeam', 'deleteAccountTeamsTeamid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountWhitelistId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountWhitelistId(accountManagementIPWhitelistId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementIPWhitelist', 'deleteAccountWhitelistId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAPaymentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAPaymentMethod(accountManagementPaymentMethodId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ns1_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccountManagementPaymentMethod', 'deleteAPaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
