# NS1 Cloud

Vendor: IBM
Homepage: https://www.ibm.com/us-en

Product: NS1 Connect
Product Page: https://www.ibm.com/products/ns1-connect

## Introduction
We classify NS1 Cloud into the Cloud domain as NS1 provides Cloud Services. 

"DNS and traffic steering solutions that empower companies to turn the workhorse of their network into an engine of innovation" 

The NS1 Cloud adapter can be integrated to the Itential Device Broker which will allow your Devices to be managed within the Itential Configuration Manager Application. 

## Why Integrate
The NS1 Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with NS1 Cloud. With this adapter you have the ability to perform operations such as:

- Traffic routing
- Zone
- Records
- Pulsar

## Additional Product Documentation
The [API documents for NS1 Cloud](https://developer.ibm.com/apis/catalog/ns1--ibm-ns1-connect-api/Introduction)